<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $table = 'driver';

    protected $fillable = [
    	'names',
    	'dni', 
    	'route_id', 
    ];

    public function route()
    {
        return $this->belongsTo('App\Route');
    }
}
