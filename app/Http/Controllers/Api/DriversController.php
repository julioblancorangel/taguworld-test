<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 

use App\Driver;
use Validator;

class DriversController extends Controller
{
    /*
	* Create New Driver
	* With params $names, $dni, $route_id
	*/
    public function createDriver(Request $request)
    {
    	$validator = Validator::make($request->all(), [ 
            'names' => 'required', 
            'dni' => 'required|unique:driver', 
            'route_id' => 'required|exists:route,id',
        ]);

        if ($validator->fails()) { 

	    	$res['success'] = false;
	        $res['error'] = $validator->errors();
	        return response($res, 200);          
        }

        $route = new Driver;
        $route->names = $request->names;
        $route->dni = $request->dni;
        $route->route_id = $request->route_id;
        $route->save();

        $res['success'] = true;
        $res['data'] = $route;

        return response($res, 200);

    }
}
