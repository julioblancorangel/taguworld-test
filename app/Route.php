<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $table = 'route';

    protected $fillable = [
    	'origin',
    	'destination', 
    ];

    public function drivers()
    {
        return $this->hasMany('App\Driver');
    }

}
