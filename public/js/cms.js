function Manager(dataApp) {
    var selfParent = this;
    this.modalCrear = $('#modal-crear');
    this.modalModif = $('#modal-modificar');

    function __construct(self) {
        
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "600",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        self.tinyInit('textarea.richtext',300,'image');
        self.tinyInit('textarea.richsm',40,'');
        self.tinyInit('textarea.richsimg',300,'');
        
    }

    this.initPagina = function (){

        this.tableContenido = $('#tableContenido').DataTable({
            "processing": true,
            "serverSide": true, //habilitar la sincronización con el servidor
            //config del paginador
            "lengthChange": true, //habilitar el cambio de numero de registros a mostrar por pagina
            "lengthMenu": [10, 25, 50, 75, 100], //cantidades del selector de records por pagina
            "pagingType": "full_numbers", //tipo de paginador
            "responsive": false,
            "deferRender": true,
            "rowReorder": false,
            "autoWidth": false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/English.json"
            },
            "ajax": {
                "type": "POST",
                "headers": {'X-CSRF-TOKEN': dataApp.contenido.token},
                "url": dataApp.contenido.read,
                "data": function (d) {}
            },
            "columns": [
                {"data": "id", "orderable": false, "width": "10%"},
                {"data": "titulo", "orderable": false, "width": "30%"},
                {"data": "pagina", "orderable": false, "width": "15%"},
                {"data": "tipo_contenido", "orderable": false, "width": "15%"},
                {"data": "orden", "orderable": false, "width": "5%"},
                {"data": "lbestatus", "orderable": false, "width": "10%"},
                {"data": "actions", "orderable": false, "width": "15%"}
            ]
        });

        $('#tableContenido').on('click', '.btn-group .btn', function () {
            var elem = $(this);
            switch (true) {
                case elem.hasClass('js-editar'):
                    selfParent.editarContenido(elem);
                    break;
                case elem.hasClass('js-eliminar'):
                    selfParent.eliminarContenido(elem);
                    break;
            }
        });

        this.tableSeo = $('#tableSeo').DataTable({
            "processing": true,
            "serverSide": true, //habilitar la sincronización con el servidor
            //config del paginador
            "lengthChange": true, //habilitar el cambio de numero de registros a mostrar por pagina
            "lengthMenu": [10, 25, 50, 75, 100], //cantidades del selector de records por pagina
            "pagingType": "full_numbers", //tipo de paginador
            "responsive": false,
            "deferRender": true,
            "rowReorder": false,
            "autoWidth": false,
            "language": {
                "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
            },
            "ajax": {
                "type": "POST",
                "headers": {'X-CSRF-TOKEN': dataApp.contenido.token},
                "url": dataApp.contenido.readSeo,
                "data": function (d) {}
            },
            "columns": [
                {"data": "id", "orderable": false, "width": "10%"},
                {"data": "detalle", "orderable": false, "width": "60%"},
                {"data": "nombre", "orderable": false, "width": "15%"},
                {"data": "actions", "orderable": false, "width": "15%"}
            ]
        });

        $('#tableSeo').on('click', '.btn-group .btn', function () {
            var elem = $(this);
            switch (true) {
                case elem.hasClass('js-editar'):
                    selfParent.AlertSolicitud(elem);
                    break;
                case elem.hasClass('js-eliminar'):
                    selfParent.eliminarSeo(elem);
                    break;
            }
        });

        this.modalCrear.on('change', '#tipo', function () {

            var tipo = $(this).val();

            if(tipo==7){
                $('.detalleDiv').prop("style").display="none";
                $('#divImg').prop("style").display="block";  
            }else{
                $('.detalleDiv').prop("style").display="block";
                $('#divImg').prop("style").display="none"; 
            }
            
        });

        this.modalCrear.on('click', '.js-crear', function () {
            var data = $('#form_mdl').serializeArray();
            selfParent.guardarSeo(data);
        });

        this.modalCrear.on('click', '.js-guardar', function () {
            var data = $('#form_mdl').serializeArray();
            selfParent.actualizarSeo(data);
        });

        $('.js-crear').on('click', function () {
            var elem = $(this);
            selfParent.AlertSolicitud(elem);
        });
        
        
    };

    return __construct(this);
}