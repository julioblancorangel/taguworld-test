function Manager(dataApp) {
    
    var selfParent = this;
    //this.modalCrear = $('#modalCreateEdit');

    function __construct(self) {
        /*
         * CODIGO NECESARIO PARA QUE EL TINYMCE FUNCIONE CORRECTAMENTE EN POPUPS
         
        $(document).on('focusin', function(e) {
            if ($(e.target).closest(".mce-window").length || $(e.target).closest(".moxman-window").length) {
                e.stopImmediatePropagation();
            }
        });
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "600",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        self.tinyInit('textarea.richtext',150);
        self.tinyInit('textarea.richtextPreg',300);
        //self.init();
        
        */
    }

    this.initClinicas = function (){

        /**
         * Funcion para obtener las especialidades pertenecientes a cada Clinica
         */
        $('.btnEspecialidades').on('click', function () {

            var idClinica = $(this).data('id-clinica');

            $.ajax({
                url: dataApp.datos.especialidad+idClinica,
                method: "GET",
                dataType: 'json',
                success: function (data) {
                    //Si la consulta retorna datos, los proceso
                    if(data['data'] != ''){
                        //For para obtener todos los items traidos por la consulta
                        data['data'].forEach( function(v, index, array) {
                            //Agrega al padre una lista de especialidades
                            var elem = $("ul[data-espec='" + idClinica +"']");
                            elem.append('<li>'+v.nombre+' <button class="btnMedicos" data-id-espec="'+v.id_spring+'">Ver Médicos</button></li>');                            
                        });

                    }else{
                        //console.log('vacio');
                    }
                }
            });
            
        });

        /**
         * Funcion para obtener los medicos segun cada especialidad
         */
        $('.btnMedicos').on('click', function () {

            var idEspec = $(this).data('id-espec');
            console.log(idEspec);
            
        });
                
    };
    
    return __construct(this);
}