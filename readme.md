## Nombre de la aplicación

Taguworld Test

## Desarrollado por:

Julio Blanco

## Metodos:

### Registro en la aplicación para obtener Token

> URL 

/api/register

> Request

```
Type: POST
Content-Type: application/json
{
	"name":"Julio Blanco",
	"email":"test@test.com",
	"password":"123456",
	"c_password":"123456"
}

```

> Response

```
{
    {
    "success": {
        "token": "TOKENAUTOGENERADO",
        "name": "Julio"
    }
}
```

### Login en la aplicación para obtener Token

> URL 

api/login

> Request

```
Type: POST
Content-Type: application/json
{
	"email":"test@test.com",
	"password":"123456"
}

```

> Response

```
{
    "success": {
        "token": "TOKENAUTOGENERADO"
    }
}
```

### Crear Ruta

> URL 

api/v1/route/create

> Request

```
Type: POST
Content-Type: application/json
Authorization: Bearer TOKENAUTOGENERADO
{
	"origin":"San Isidro",
	"destination":"Miraflores"
}

```

> Response

```
{
    "success": true,
    "data": {
        "origin": "San Isidro",
        "destination": "Miraflores",
        "updated_at": "2018-10-28 18:31:44",
        "created_at": "2018-10-28 18:31:44",
        "id": 1
    }
}
```

### Crear Chofer y Asignar Ruta

> URL 

api/v1/driver/create

> Request

```
Type: POST
Content-Type: application/json
Authorization: Bearer TOKENAUTOGENERADO
{
	"names":"Julio Blanco",
	"dni":"139079982",
	"route_id": 1
}

```

> Response

```
{
    "success": true,
    "data": {
        "names":"Julio Blanco",
		"dni":"139079982",
        "route_id": "1",
        "updated_at": "2018-10-28 18:26:36",
        "created_at": "2018-10-28 18:26:36",
        "id": 1
    }
}
```

### Obtiene lista de todas las rutas

> URL 

api/v1/route/list

> Request

```
Type: GET
Content-Type: application/json

```

> Response

```
{
    "success": true,
    "data": [
        {
            "id": 1,
            "origin": "San Isidro",
            "destination": "Miraflores",
            "created_at": "2018-10-28 18:09:50",
            "updated_at": "2018-10-28 18:09:50"
        },
        {
            "id": 2,
            "origin": "Pueblo Libre",
            "destination": "Barranco",
            "created_at": "2018-10-28 18:31:44",
            "updated_at": "2018-10-28 18:31:44"
        }
    ]
}
```

### Obtiene lista de conductores de una ruta

> URL 

api/v1/route/drivers-route/{id_ruta}

> Request

```
Type: GET
Content-Type: application/json

```

> Response

```
{
    "success": true,
    "data": {
        "route": [
            {
                "id": 1,
                "origin": "San Isidro",
                "destination": "Miraflores",
                "created_at": "2018-10-28 18:09:50",
                "updated_at": "2018-10-28 18:09:50"
            }
        ],
        "drivers": [
            {
                "id": 1,
                "names": "Julio Blanco",
                "dni": "139079982",
                "route_id": 1,
                "created_at": "2018-10-28 18:17:14",
                "updated_at": "2018-10-28 18:17:14"
            },
            {
                "id": 2,
                "names": "Pierina Sora",
                "dni": "141024042",
                "route_id": 1,
                "created_at": "2018-10-28 18:26:36",
                "updated_at": "2018-10-28 18:26:36"
            }
        ]
    }
}
```