@extends('layouts.home')

@section('container')

<!-- Row -->
<div class="row">
    <div class="col-12">
	    <div class="card">
	        <div class="card-body">
	            <h4 class="card-title">Route Details</h4>
	            <h4 class="card-subtitle"><b>Origin: </b>{{$route->origin}} | <b>Destination: </b>{{$route->origin}}</h4>
	            <div class="table-responsive">
	                <table id="myTable" class="table table-bordered table-striped">
	                    <thead>
	                        <tr>
	                            <th>Names</th>
	                            <th>DNI</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@if (count($route->drivers) > 0)
		                        @foreach ($route->drivers as $rou)
		                            <tr>
		                                <td>{{ $rou->names }}</td>
		                                <td>{{ $rou->dni }}</td>
		                            </tr>
		                        @endforeach
		                    @else
		                        <tr>
		                            <td colspan="3">Theres is no drivers availables</td>
		                        </tr>
		                    @endif
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>    
</div>
<!-- Row -->  
@endsection

@section('scripts-pagina')
<!-- This is data table -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script>
$(document).ready(function() {
    $('#myTable').DataTable();
});
</script>
@endsection