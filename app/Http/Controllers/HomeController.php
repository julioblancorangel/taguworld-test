<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Route;
use App\Driver;

class HomeController extends Controller
{
	/*
	* Vista de Inicio que muestra la lista de rutas
	*
	*/
    public function index()
    {
        $routes = Route::with('drivers')->orderBy('id', 'asc')->get();
        //dd($routes[0]->drivers);
    	return view('index')->with('routes', $routes);
    }

    /*
	* Detalle de Ruta
	*
	*/
    public function routeDetail($id)
    {
        
        $route = Route::where('id', $id)->with('drivers')->orderBy('id', 'asc')->first();
        
        if(isset($route)){
            return view('detail-route')->with('route', $route);
        }else{
            abort(404);
        }
    	
    }

}
