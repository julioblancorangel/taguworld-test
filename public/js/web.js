function Manager(dataApp) {
    
    var selfParent = this;
    //this.modalCrear = $('#modalCreateEdit');

    function __construct(self) {
        /*
         * CODIGO NECESARIO PARA QUE EL TINYMCE FUNCIONE CORRECTAMENTE EN POPUPS
         
        $(document).on('focusin', function(e) {
            if ($(e.target).closest(".mce-window").length || $(e.target).closest(".moxman-window").length) {
                e.stopImmediatePropagation();
            }
        });
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "600",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        self.tinyInit('textarea.richtext',150);
        self.tinyInit('textarea.richtextPreg',300);
        //self.init();
        
        */
    }

    this.initBuscarMedico = function (){
        
        /**
         * Cambia valor de campo segun la seleccion de especialidad
         */
        $('#id-espec').on('change', function (e) {
            //e.preventDefault();
            var nombreEsp = $(this).find(':selected').data('nombre')
            $('#nombEsp').val(nombreEsp);
        });
      
    };

    return __construct(this);
}