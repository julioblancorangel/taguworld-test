@extends('layouts.home')

@section('container')

<!-- Row -->
<div class="row">
    <div class="col-12">
	    <div class="card">
	        <div class="card-body">
	            <h4 class="card-title">List of Routes</h4>
	            <div class="table-responsive">
	                <table id="myTable" class="table table-bordered table-striped">
	                    <thead>
	                        <tr>
	                            <th>Origin</th>
	                            <th>Destination</th>
	                            <th>Actions</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                    	@if (count($routes) > 0)
		                        @foreach ($routes as $rou)
		                            <tr>
		                                <td>{{ $rou->origin }}</td>
		                                <td>{{ $rou->destination }}</td>
		                                <td>
		                                    <div class="btn-group pull-right">
		                                        <a href="{{ route('detalle_ruta',[$rou->id]) }}" class="btn btn-sm btn-primary">Details</a>
		                                    </div>
		                                </td>

		                            </tr>
		                        @endforeach
		                    @else
		                        <tr>
		                            <td colspan="3">Theres is no routes availables</td>
		                        </tr>
		                    @endif
	                    </tbody>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>    
</div>
<!-- Row -->  
@endsection

@section('scripts-pagina')
<!-- This is data table -->
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script>
$(document).ready(function() {
    $('#myTable').DataTable();
});
</script>
@endsection