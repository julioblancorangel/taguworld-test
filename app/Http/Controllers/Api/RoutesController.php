<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 

use App\Route;
use Validator;

class RoutesController extends Controller
{
    	
	/*
	* List of Routes with Origin and Destination
	*/
    public function listRoutes()
    {
    	$data = Route::orderBy('id', 'asc')->get();
    	$res['success'] = true;
        $res['data'] = $data;

        return response($res, 200);

    }	

    /*
	* List of Routes with Origin and Destination
	*/
    public function listDriversRoute($id)
    {
    	//$data = Route::orderBy('id', 'asc')->get();
    	
    	$ruta = Route::where('id', $id)->get();

    	if(count($ruta) > 0){

    		$drivers = Route::join('driver', 'driver.route_id', '=', 'route.id')
            ->where(['route.id' => $id])
            ->orderBy('route.id')
            ->get(['driver.*']);

	        $data['route'] = $ruta;
	        $data['drivers'] = $drivers;

	    	$res['success'] = true;
	        $res['data'] = $data;

    	}else{

	        $data = 'Esta ruta no existe o no es válida';
	    	$res['success'] = false;
	        $res['data'] = $data;
    	}
    	
        return response($res, 200);

    }

    /*
	* Create New Route
	* With params $origin, $destination
	*/
    public function createRoute(Request $request)
    {
    	$validator = Validator::make($request->all(), [ 
            'origin' => 'required', 
            'destination' => 'required', 
        ]);

        if ($validator->fails()) { 

	    	$res['success'] = false;
	        $res['error'] = $validator->errors();
	        return response($res, 200);          
        }

        $route = new Route;
        $route->origin = $request->origin;
        $route->destination = $request->destination;
        $route->save();

        $res['success'] = true;
        $res['data'] = $route;

        return response($res, 200);

    }

}
